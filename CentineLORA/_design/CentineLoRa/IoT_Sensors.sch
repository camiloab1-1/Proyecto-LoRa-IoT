EESchema Schematic File Version 4
EELAYER 30 0
EELAYER END
$Descr User 8889 5960
encoding utf-8
Sheet 2 4
Title "IoT Sensors -- CentineLoRa"
Date "2020-11-29"
Rev "0.1.1"
Comp "Universidad Sergio Arboleda"
Comment1 "Camilo Arzuza Bonett"
Comment2 "Luis Alejandro Rios"
Comment3 ""
Comment4 ""
$EndDescr
$Comp
L L50:L50 U7
U 1 1 5FA07E35
P 2760 3025
F 0 "U7" H 2760 3892 50  0000 C CNN
F 1 "L50" H 2760 3801 50  0000 C CNN
F 2 "modules_lib:MODULE_L50" H 2760 3025 50  0001 L BNN
F 3 "2.50 mm" H 2760 3025 50  0001 L BNN
F 4 "2.0" H 2760 3025 50  0001 L BNN "Field4"
F 5 "Quectel" H 2760 3025 50  0001 L BNN "Field5"
F 6 "Manufacturer Recommendations" H 2760 3025 50  0001 L BNN "Field6"
	1    2760 3025
	1    0    0    -1  
$EndComp
Text HLabel 1735 2725 0    50   BiDi ~ 0
~RESET
Text HLabel 1735 2825 0    50   BiDi ~ 0
EITO
Text HLabel 1735 2925 0    50   BiDi ~ 0
ON_OFF
Text HLabel 1735 3125 0    50   BiDi ~ 0
CFG0_SCK
Text HLabel 1735 3225 0    50   BiDi ~ 0
CFG1_SCS
Text HLabel 1735 3325 0    50   BiDi ~ 0
RxD_MOSI_SDA
Text HLabel 1735 3425 0    50   BiDi ~ 0
TxD_MISO_SCL
$Comp
L power:GND #PWR0152
U 1 1 5FA094A4
P 3750 3700
F 0 "#PWR0152" H 3750 3450 50  0001 C CNN
F 1 "GND" H 3755 3527 50  0000 C CNN
F 2 "" H 3750 3700 50  0001 C CNN
F 3 "" H 3750 3700 50  0001 C CNN
	1    3750 3700
	1    0    0    -1  
$EndComp
Text HLabel 3780 2525 2    50   BiDi ~ 0
VIO_RTC
Text HLabel 3780 2825 2    50   BiDi ~ 0
1PPS
Text HLabel 3780 3025 2    50   BiDi ~ 0
DR_I2C_CLK
Text HLabel 3780 3125 2    50   BiDi ~ 0
DR_I2C_DIO
Wire Wire Line
	1860 2725 1735 2725
Wire Wire Line
	1735 2825 1860 2825
Wire Wire Line
	1735 2925 1860 2925
Wire Wire Line
	1735 3125 1860 3125
Wire Wire Line
	1735 3225 1860 3225
Wire Wire Line
	3660 2525 3780 2525
Wire Wire Line
	3660 2825 3780 2825
Wire Wire Line
	3660 3025 3780 3025
Wire Wire Line
	3660 3125 3780 3125
Wire Wire Line
	3660 3625 3750 3625
Wire Wire Line
	3750 3625 3750 3700
$Comp
L power:+3.3V #PWR0153
U 1 1 5FA0CB8D
P 3760 2335
F 0 "#PWR0153" H 3760 2185 50  0001 C CNN
F 1 "+3.3V" H 3775 2508 50  0000 C CNN
F 2 "" H 3760 2335 50  0001 C CNN
F 3 "" H 3760 2335 50  0001 C CNN
	1    3760 2335
	1    0    0    -1  
$EndComp
Wire Wire Line
	3760 2335 3760 2425
Wire Wire Line
	3760 2425 3660 2425
Wire Notes Line
	1030 2030 4345 2030
Wire Notes Line
	4345 2030 4345 3955
Wire Notes Line
	4345 3955 1030 3955
Wire Notes Line
	1030 2030 1030 3955
Text Notes 630  1965 0    118  ~ 24
GPS Module with embedded ceramic antenna
$Comp
L Sensor:BME680 U8
U 1 1 5FBA5769
P 6350 2875
F 0 "U8" H 5921 2921 50  0000 R CNN
F 1 "BME680" H 5921 2830 50  0000 R CNN
F 2 "Package_LGA:Bosch_LGA-8_3x3mm_P0.8mm_ClockwisePinNumbering" H 7800 2425 50  0001 C CNN
F 3 "https://ae-bst.resource.bosch.com/media/_tech/media/datasheets/BST-BME680-DS001.pdf" H 6350 2675 50  0001 C CNN
	1    6350 2875
	1    0    0    -1  
$EndComp
$Comp
L power:+3.3V #PWR0160
U 1 1 5FBA6961
P 6345 2175
F 0 "#PWR0160" H 6345 2025 50  0001 C CNN
F 1 "+3.3V" H 6360 2348 50  0000 C CNN
F 2 "" H 6345 2175 50  0001 C CNN
F 3 "" H 6345 2175 50  0001 C CNN
	1    6345 2175
	1    0    0    -1  
$EndComp
Wire Wire Line
	6450 2275 6450 2225
Wire Wire Line
	6450 2225 6345 2225
Wire Wire Line
	6250 2225 6250 2275
Wire Wire Line
	6345 2225 6345 2175
Connection ~ 6345 2225
Wire Wire Line
	6345 2225 6250 2225
$Comp
L power:GND #PWR0161
U 1 1 5FBA841B
P 6355 3615
F 0 "#PWR0161" H 6355 3365 50  0001 C CNN
F 1 "GND" H 6360 3442 50  0000 C CNN
F 2 "" H 6355 3615 50  0001 C CNN
F 3 "" H 6355 3615 50  0001 C CNN
	1    6355 3615
	1    0    0    -1  
$EndComp
Wire Wire Line
	6250 3475 6250 3535
Wire Wire Line
	6250 3535 6355 3535
Wire Wire Line
	6450 3535 6450 3475
Wire Wire Line
	6355 3535 6355 3615
Connection ~ 6355 3535
Wire Wire Line
	6355 3535 6450 3535
Text HLabel 7145 2575 2    50   BiDi ~ 0
MISO
Text HLabel 7145 2775 2    50   BiDi ~ 0
SCK
Text HLabel 7145 2975 2    50   BiDi ~ 0
MOSI
Text HLabel 7145 3175 2    50   BiDi ~ 0
SS
Wire Wire Line
	7145 2575 6950 2575
Wire Wire Line
	6950 2775 7145 2775
Wire Wire Line
	6950 2975 7145 2975
Wire Wire Line
	6950 3175 7145 3175
Wire Notes Line
	7545 1915 7545 3945
Wire Notes Line
	7545 3945 5520 3945
Wire Notes Line
	5520 3945 5520 1915
Wire Notes Line
	5520 1915 7545 1915
Text Notes 5120 1825 0    118  ~ 24
Air Quality, Humidity\nPressure and Temperature Sensor\nBME680
Wire Wire Line
	1860 3325 1735 3325
Wire Wire Line
	1860 3425 1735 3425
$EndSCHEMATC

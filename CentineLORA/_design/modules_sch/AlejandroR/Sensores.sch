EESchema Schematic File Version 4
EELAYER 30 0
EELAYER END
$Descr USLetter 11000 8500
encoding utf-8
Sheet 1 1
Title "Sensor Peripherals"
Date "2020-11-01"
Rev "0.1.0"
Comp "Universidad Sergio Arboleda"
Comment1 "Camilo Bonett"
Comment2 "Alejandro Rios"
Comment3 ""
Comment4 ""
$EndDescr
$Comp
L L50:L50 U1
U 1 1 5FA07E35
P 2730 2090
F 0 "U1" H 2730 2957 50  0000 C CNN
F 1 "L50" H 2730 2866 50  0000 C CNN
F 2 "MODULE_L50" H 2730 2090 50  0001 L BNN
F 3 "2.50 mm" H 2730 2090 50  0001 L BNN
F 4 "2.0" H 2730 2090 50  0001 L BNN "Field4"
F 5 "Quectel" H 2730 2090 50  0001 L BNN "Field5"
F 6 "Manufacturer Recommendations" H 2730 2090 50  0001 L BNN "Field6"
	1    2730 2090
	1    0    0    -1  
$EndComp
Text HLabel 1705 1790 0    50   BiDi ~ 0
~RESET
Text HLabel 1705 1890 0    50   BiDi ~ 0
EITO
Text HLabel 1705 1990 0    50   BiDi ~ 0
ON_OFF
Text HLabel 1705 2190 0    50   BiDi ~ 0
CFG0_SCK
Text HLabel 1705 2290 0    50   BiDi ~ 0
CFG1_SCS
Text HLabel 1705 2390 0    50   BiDi ~ 0
RXD_MOSI_SDA
Text HLabel 1705 2490 0    50   BiDi ~ 0
TXD_MISO_SCL
$Comp
L power:GND #PWR?
U 1 1 5FA094A4
P 3720 2765
F 0 "#PWR?" H 3720 2515 50  0001 C CNN
F 1 "GND" H 3725 2592 50  0000 C CNN
F 2 "" H 3720 2765 50  0001 C CNN
F 3 "" H 3720 2765 50  0001 C CNN
	1    3720 2765
	1    0    0    -1  
$EndComp
Text HLabel 3750 1590 2    50   BiDi ~ 0
VIO_RTC
Text HLabel 3750 1890 2    50   BiDi ~ 0
1PPS
Text HLabel 3750 2090 2    50   BiDi ~ 0
DR_I2C_CLK
Text HLabel 3750 2190 2    50   BiDi ~ 0
DR_I2C_CLK
Wire Wire Line
	1830 1790 1705 1790
Wire Wire Line
	1705 1890 1830 1890
Wire Wire Line
	1705 1990 1830 1990
Wire Wire Line
	1705 2190 1830 2190
Wire Wire Line
	1705 2290 1830 2290
Wire Wire Line
	1705 2390 1830 2390
Wire Wire Line
	1705 2490 1830 2490
Wire Wire Line
	3630 1590 3750 1590
Wire Wire Line
	3630 1890 3750 1890
Wire Wire Line
	3630 2090 3750 2090
Wire Wire Line
	3630 2190 3750 2190
Wire Wire Line
	3630 2690 3720 2690
Wire Wire Line
	3720 2690 3720 2765
$Comp
L power:+3.3V #PWR?
U 1 1 5FA0CB8D
P 3730 1400
F 0 "#PWR?" H 3730 1250 50  0001 C CNN
F 1 "+3.3V" H 3745 1573 50  0000 C CNN
F 2 "" H 3730 1400 50  0001 C CNN
F 3 "" H 3730 1400 50  0001 C CNN
	1    3730 1400
	1    0    0    -1  
$EndComp
Wire Wire Line
	3730 1400 3730 1490
Wire Wire Line
	3730 1490 3630 1490
Wire Notes Line
	1000 1095 4315 1095
Wire Notes Line
	4315 1095 4315 3020
Wire Notes Line
	4315 3020 1000 3020
Wire Notes Line
	1000 1095 1000 3020
Text Notes 1615 1035 0    50   ~ 10
Modulo GPS con antena ceramica integrada -- Camilo AB
$Comp
L Sensor:BME680 U2
U 1 1 5FBA5769
P 5900 2035
F 0 "U2" H 5471 2081 50  0000 R CNN
F 1 "BME680" H 5471 1990 50  0000 R CNN
F 2 "Package_LGA:Bosch_LGA-8_3x3mm_P0.8mm_ClockwisePinNumbering" H 7350 1585 50  0001 C CNN
F 3 "https://ae-bst.resource.bosch.com/media/_tech/media/datasheets/BST-BME680-DS001.pdf" H 5900 1835 50  0001 C CNN
	1    5900 2035
	1    0    0    -1  
$EndComp
$Comp
L power:+3.3V #PWR?
U 1 1 5FBA6961
P 5895 1335
F 0 "#PWR?" H 5895 1185 50  0001 C CNN
F 1 "+3.3V" H 5910 1508 50  0000 C CNN
F 2 "" H 5895 1335 50  0001 C CNN
F 3 "" H 5895 1335 50  0001 C CNN
	1    5895 1335
	1    0    0    -1  
$EndComp
Wire Wire Line
	6000 1435 6000 1385
Wire Wire Line
	6000 1385 5895 1385
Wire Wire Line
	5800 1385 5800 1435
Wire Wire Line
	5895 1385 5895 1335
Connection ~ 5895 1385
Wire Wire Line
	5895 1385 5800 1385
$Comp
L power:GND #PWR?
U 1 1 5FBA841B
P 5905 2775
F 0 "#PWR?" H 5905 2525 50  0001 C CNN
F 1 "GND" H 5910 2602 50  0000 C CNN
F 2 "" H 5905 2775 50  0001 C CNN
F 3 "" H 5905 2775 50  0001 C CNN
	1    5905 2775
	1    0    0    -1  
$EndComp
Wire Wire Line
	5800 2635 5800 2695
Wire Wire Line
	5800 2695 5905 2695
Wire Wire Line
	6000 2695 6000 2635
Wire Wire Line
	5905 2695 5905 2775
Connection ~ 5905 2695
Wire Wire Line
	5905 2695 6000 2695
Text HLabel 6695 1735 2    50   BiDi ~ 0
MISO
Text HLabel 6695 1935 2    50   BiDi ~ 0
SCK
Text HLabel 6695 2135 2    50   BiDi ~ 0
MOSI
Text HLabel 6695 2335 2    50   BiDi ~ 0
IO-15
Wire Wire Line
	6695 1735 6500 1735
Wire Wire Line
	6500 1935 6695 1935
Wire Wire Line
	6500 2135 6695 2135
Wire Wire Line
	6500 2335 6695 2335
Wire Notes Line
	7095 1075 7095 3105
Wire Notes Line
	7095 3105 5070 3105
Wire Notes Line
	5070 3105 5070 1075
Wire Notes Line
	5070 1075 7095 1075
Text Notes 5125 1010 0    50   ~ 10
Senor de Calidad de Aire BME630 -- Camilo AB
$EndSCHEMATC
